import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faUsers,
  faSignInAlt,
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";

const Navbar = ({ hasRolAdmin, isUserLogged, onHandleShowNavbar }) => {
  const [state, setState] = useState({
    hasRolAdmin: false,
    isUserLogged: false,
  });

  const navigate = useNavigate();

  useEffect(() => {
    setState({
      ...state,
      hasRolAdmin: hasRolAdmin,
      isUserLogged: isUserLogged,
    });

    if (!isUserLogged) navigate("/");
  }, []);

  const handleLogout = () => {
    window.sessionStorage.removeItem("AutorizationToken");
    onHandleShowNavbar();
    navigate("/");
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <a className="navbar-brand" href="#">
        <Link to="/" style={{ color: "white" }}>
          Sophos Kanban
        </Link>
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav ml-auto">
          {state.isUserLogged && (
            <li className="nav-item active">
              <a className="nav-link">
                <Link to="/kanban" style={{ color: "white" }}>
                  <FontAwesomeIcon icon={faHome} className="mr-2" />
                  Kanban
                </Link>
              </a>
            </li>
          )}
          {state.isUserLogged && state.hasRolAdmin && (
            <li className="nav-item active">
              <a className="nav-link">
                <Link to="/users" style={{ color: "white" }}>
                  <FontAwesomeIcon icon={faUsers} className="mr-2" />
                  Usuarios
                </Link>
              </a>
            </li>
          )}
          <li className="nav-item">
            <a className="nav-link">
              {!state.isUserLogged && (
                <Link to="/login" style={{ color: "white" }}>
                  <FontAwesomeIcon icon={faSignInAlt} className="mr-2" />
                  Iniciar Sesión
                </Link>
              )}
              {state.isUserLogged && (
                <button
                  className="link-light"
                  style={{
                    color: "white",
                    border: "none",
                    background: "transparent",
                  }}
                  onClick={handleLogout}
                >
                  <FontAwesomeIcon icon={faSignInAlt} className="mr-2" />
                  Cerrar Sesión
                </button>
              )}
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
