import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useSnackbar } from "react-simple-snackbar";
import { DndContext, DropTarget, DragSource } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import axios from "axios";
import { decode as atob, encode as btoa } from "base-64";

const Kanban = ({ onHandleShowNavbar }) => {
  const navigate = useNavigate();

  const [openSnack, closeSnack] = useSnackbar({
    duration: 500,
    position: "top-right",
    style: { backgroundColor: "green", marginTop: "2rem" },
  });

  const [openSnackErr, closeSnackErr] = useSnackbar({
    duration: 500,
    position: "top-right",
    style: { backgroundColor: "red", marginTop: "2rem" },
  });

  const [state, setState] = useState({
    tasks: [{}],
    labels: [],
    token: "",
  });

  useEffect(() => {
    handleGetInitialData();
  }, []);

  const handleGetInitialData = async () => {
    const stringToken = window.sessionStorage.getItem("AutorizationToken");
    if (!stringToken) {
      handleLogout();
    } else {
      const decodedToken = atob(stringToken);
      setState({ ...state, token: decodedToken });
      const splitedToken = decodedToken.split(".");
      const rolesToken = atob(splitedToken[1]);
      const parsedUserInfo = JSON.parse(rolesToken);
      const parsedRoles = JSON.parse(rolesToken)["roles"];
      let tasks;
      let labels;
      if (parsedRoles.includes("ROLE_ADMIN")) {
        tasks = await axios
          .get(`${process.env.REACT_APP_API_URL}task/all`, {
            headers: {
              Authorization: `Bearer ${decodedToken}`,
            },
          })
          .then((res) => {
            return res.data;
          });

        labels = await axios
          .get(`${process.env.REACT_APP_API_URL}status/all`, {
            headers: {
              Authorization: `Bearer ${decodedToken}`,
            },
          })
          .then((res) => {
            return res.data;
          });
      } else {
        tasks = await axios
          .get(
            `${process.env.REACT_APP_API_URL}task/byUsername/${parsedUserInfo["sub"]}`,
            {
              headers: {
                Authorization: `Bearer ${decodedToken}`,
              },
            }
          )
          .then((res) => {
            return res.data;
          });

        labels = await axios
          .get(`${process.env.REACT_APP_API_URL}status/all`, {
            headers: {
              Authorization: `Bearer ${decodedToken}`,
            },
          })
          .then((res) => {
            return res.data;
          });
      }
      setState({ ...state, tasks, labels });
    }
  };

  const handleLogout = () => {
    window.sessionStorage.removeItem("AutorizationToken");
    onHandleShowNavbar();
    navigate("/");
  };

  const updateStatus = async (infoTask, newStatus) => {
    const body = {
      name: infoTask?.name,
      description: infoTask?.description,
      userId: infoTask?.userId,
      username: infoTask?.username,
      status: parseInt(newStatus),
    };
    await axios
      .post(
        `${process.env.REACT_APP_API_URL}task/update/${infoTask?.id}`,
        body,
        {
          headers: {
            Authorization: `Bearer ${state.token}`,
            "Content-Type": "application/json",
          },
        }
      )
      .then(() => {
        openSnack("Estado actualizado");
      })
      .catch((error) => {
        openSnackErr("Error al actualizar el estado");
      });
  };

  return (
    <div className="mt-4 card">
      <h3 className="card-title text-center mt-4">Kanban</h3>
      <div className="row">
        {state.labels.map((label) => {
          return (
            <div className="col-2">
              <div className="card">
                <div className="card-header text-center">{label.name}</div>
                <div className="card-body">
                  {state.tasks.map((task) => {
                    if (task.status === label.id) {
                      return (
                        <div className="card mb-2">
                          <h5 class="card-header text-center">{task.name}</h5>
                          <p class="card-text text-justify">
                            Descripción: {task.description}
                          </p>
                          <p class="card-text">Usuario: {task.username}</p>
                          <select
                            onChange={(e) => updateStatus(task, e.target.value)}
                          >
                            <option selected disabled>
                              Cambiar de estado:
                            </option>
                            {state.labels.map((label_select) => {
                              if (label_select.id !== task.status) {
                                return (
                                  <option value={label_select.id}>
                                    {label_select.name}
                                  </option>
                                );
                              }
                            })}
                          </select>
                        </div>
                      );
                    }
                  })}
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Kanban;
