import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { useSnackbar } from "react-simple-snackbar";
import { decode as atob, encode as btoa } from "base-64";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Login = (props) => {
  const [state, setState] = useState({
    username: undefined,
    password: undefined,
    showHidePassword: false,
    errors: [],
  });

  const navigate = useNavigate();

  const [openSnack, closeSnack] = useSnackbar({
    duration: 500,
    position: "top-right",
    style: { backgroundColor: "green", marginTop: "2rem" },
  });
  const [openSnackErr, closeSnackErr] = useSnackbar({
    duration: 500,
    position: "top-right",
    style: { backgroundColor: "red", marginTop: "2rem" },
  });

  const handleHasError = (key) => {
    return state.errors.indexOf(key) !== -1;
  };

  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setState({ ...state, [name]: value });
  };

  const handelChangeShowHidePassword = (event) => {
    const actualValue = !state.showHidePassword;
    setState({ ...state, showHidePassword: actualValue });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const errors = [];

    if (state.username === "" || state.username === undefined) {
      errors.push("username");
    }

    if (state.password === "" || state.password === undefined) {
      errors.push("password");
    }

    setState({ ...state, errors });

    if (errors.length > 0) {
      return false;
    } else {
      const body = {
        username: state.username,
        password: state.password,
      };
      await axios
        .post(`${process.env.REACT_APP_API_URL}auth/login`, body)
        .then((res) => {
          const resCoded = btoa(res?.data?.token);
          window.sessionStorage.setItem("AutorizationToken", resCoded);
          openSnack("Bienvenido");
        })
        .then(() => {
          navigate("/kanban");
          props.onHandleShowNavbar();
        })
        .catch((error) => {
          console.error(error);
          openSnackErr(error.message);
        });
    }
  };

  return (
    <div className="mt-4 card">
      <h5 className="card-title text-center mt-4">Iniciar sesión</h5>
      <form className="pl-4 pr-4 pb-4 row">
        <div className="col-lg-12">
          <label htmlFor="username">Usuario</label>
          <input
            autoComplete="off"
            className={
              handleHasError("username")
                ? "form-control is-invalid"
                : "form-control"
            }
            name="username"
            placeholder="Usuario"
            defaultValue={state.username}
            value={state.username}
            onChange={handleInputChange}
          />
          <div
            className={
              handleHasError("username")
                ? "message-error-class"
                : "hidden-class"
            }
          >
            Por favor, ingrese un valor.
          </div>
        </div>
        <div className="mt-3 col-lg-12">
          <label htmlFor="password">Password</label>
          <div className="input-group">
            <input
              autoComplete="off"
              className={
                handleHasError("password")
                  ? "form-control is-invalid"
                  : "form-control"
              }
              name="password"
              defaultValue={state.password}
              value={state.password}
              onChange={handleInputChange}
              type={state.showHidePassword ? "text" : "password"}
              placeholder="Contraseña"
              aria-describedby="button-addon2"
            />
            <button
              className="btn btn-outline-secondary"
              type="button"
              id="button-addon2"
              onClick={handelChangeShowHidePassword}
            >
              {state.showHidePassword ? (
                <FontAwesomeIcon icon={faEyeSlash} />
              ) : (
                <FontAwesomeIcon icon={faEye} />
              )}
            </button>
          </div>
          <div
            className={
              handleHasError("password")
                ? "message-error-class"
                : "hidden-class"
            }
          >
            Por favor ingrese una contraseña.
          </div>
        </div>
        <div className="mt-4 col-lg-12 padd-top text-center">
          <button className="btn btn-success" onClick={handleSubmit}>
            Iniciar sesión
          </button>
        </div>
      </form>
    </div>
  );
};

export default Login;
