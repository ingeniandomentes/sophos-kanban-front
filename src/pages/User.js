import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { decode as atob, encode as btoa } from "base-64";

const User = ({ onHandleShowNavbar }) => {
  const navigate = useNavigate();

  useEffect(() => {
    const stringToken = window.sessionStorage.getItem("AutorizationToken");
    if (stringToken) {
      const decodedToken = atob(stringToken);
      const splitedToken = decodedToken.split(".");
      const rolesToken = atob(splitedToken[1]);
      const parsedRoles = JSON.parse(rolesToken)["roles"];
      console.log(parsedRoles.includes("ROLE_ADMIN"));
      if (!parsedRoles.includes("ROLE_ADMIN")) {
        handleLogout();
      }
    } else {
      handleLogout();
    }
  }, []);

  const handleLogout = () => {
    window.sessionStorage.removeItem("AutorizationToken");
    onHandleShowNavbar();
    navigate("/");
  };
  return (
    <div>
      <h1>User section</h1>
    </div>
  );
};

export default User;
