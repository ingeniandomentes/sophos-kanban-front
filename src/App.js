import React, { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Navbar from "./components/partials/Navbar";
import Login from "./pages/Login";
import SnackbarProvider from "react-simple-snackbar";
import Kanban from "./pages/Kanban";
import { decode as atob, encode as btoa } from "base-64";
import User from "./pages/User";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasRolAdmin: false,
      isUserLogged: false,
      showNavbar: false,
    };
  }

  async componentDidMount() {
    this.handleInitialState();
  }

  handleInitialState = async () => {
    this.setState({ ...this.state, showNavbar: false }, () => {
      const stringToken = window.sessionStorage.getItem("AutorizationToken");
      if (stringToken) {
        const decodedToken = atob(stringToken);
        const splitedToken = decodedToken.split(".");
        const rolesToken = atob(splitedToken[1]);
        const parsedRoles = JSON.parse(rolesToken)["roles"];
        if (parsedRoles.includes("ROLE_ADMIN")) {
          this.setState(
            { ...this.state, hasRolAdmin: true, isUserLogged: true },
            () => {
              this.setState({ ...this.state, showNavbar: true });
            }
          );
        } else {
          this.setState(
            { ...this.state, hasRolAdmin: false, isUserLogged: true },
            () => {
              this.setState({ ...this.state, showNavbar: true });
            }
          );
        }
      } else {
        this.setState(
          { ...this.state, hasRolAdmin: false, isUserLogged: false },
          () => {
            this.setState({ ...this.state, showNavbar: true });
          }
        );
      }
    });
  };

  render() {
    return (
      <SnackbarProvider>
        <BrowserRouter>
          {this.state.showNavbar && (
            <Navbar
              hasRolAdmin={this.state.hasRolAdmin}
              isUserLogged={this.state.isUserLogged}
              onHandleShowNavbar={this.handleInitialState}
            />
          )}
          <div className="container">
            <Routes>
              <Route
                path="/login"
                element={<Login onHandleShowNavbar={this.handleInitialState} />}
              />
              <Route
                path="/kanban"
                element={
                  <Kanban onHandleShowNavbar={this.handleInitialState} />
                }
              />
              <Route
                path="/users"
                element={<User onHandleShowNavbar={this.handleInitialState} />}
              />
              <Route
                path="/"
                element={<Login onHandleShowNavbar={this.handleInitialState} />}
              />
            </Routes>
          </div>
        </BrowserRouter>
      </SnackbarProvider>
    );
  }
}

export default App;
